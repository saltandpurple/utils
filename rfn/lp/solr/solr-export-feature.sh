#! /bin/bash
BUCKET=s3://fp-dev-solr-data
PROFILE=rfn-fp-dev
SSH_HOST=10.127.8.177
ENVIRONMENT=feature

rm -r ./solr-${ENVIRONMENT}/
mkdir -p ./solr-${ENVIRONMENT}/tnt/
mkdir -p ./solr-${ENVIRONMENT}/rhepro/
mkdir -p ./solr-${ENVIRONMENT}/weborder/

echo "Starting TnT solr export"
echo "Copying tntDelivery..."
sshpass -p "!idealist10" scp -r idealist@${SSH_HOST}:~/portal/solr-tnt/server/solr/tntDelivery solr-${ENVIRONMENT}/tnt/tntDelivery
echo "Copying tntMasterorder..."
sshpass -p "!idealist10" scp -r idealist@${SSH_HOST}:~/portal/solr-tnt/server/solr/tntMasterorder solr-${ENVIRONMENT}/tnt/tntMasterorder
echo "Syncing tnt data with s3 bucket..."
aws s3 sync ./solr-${ENVIRONMENT}/tnt/ $BUCKET/tnt --profile $PROFILE
echo "Completed tnt solr export."

echo "Starting RhePro solr export"
echo "Copying rpAddressBook..."
sshpass -p "!idealist10" scp -r idealist@${SSH_HOST}:~/portal/solr-rhepro/server/solr/rpAddressBook solr-${ENVIRONMENT}/rhepro/rpAddressBook
echo "Copying rpPickupOrder..."
sshpass -p "!idealist10" scp -r idealist@${SSH_HOST}:~/portal/solr-rhepro/server/solr/rpPickupOrder solr-${ENVIRONMENT}/rhepro/rpPickupOrder
echo "Syncing RhePro data with s3 bucket..."
aws s3 sync ./solr-${ENVIRONMENT}/rhepro/ $BUCKET/rhepro --profile $PROFILE
echo "Completed RhePro solr export."

echo "Starting Weborder solr export"
echo "Copying woAddressBook..."
sshpass -p "!idealist10" scp -r idealist@${SSH_HOST}:~/portal/solr-weborder/server/solr/woAddressBook solr-${ENVIRONMENT}/weborder/woAddressBook
echo "Copying woShipment..."
sshpass -p "!idealist10" scp -r idealist@${SSH_HOST}:~/portal/solr-weborder/server/solr/woShipment solr-${ENVIRONMENT}/weborder/woShipment
echo "Syncing Weborder data with s3 bucket..."
aws s3 sync ./solr-${ENVIRONMENT}/weborder/ $BUCKET/weborder --profile $PROFILE
echo "Completed Weborder solr export."

echo "Completed solr export to s3 bucket."

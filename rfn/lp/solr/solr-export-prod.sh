jumpnode: 10.4.133.11
solrnode: 10.127.8.4
bucket: s3://lp-prod-solr-data
PROFILE=rfn-ss
SSH_HOST=10.127.8.4
ENVIRONMENT=prod

rm -r ./solr-${ENVIRONMENT}/
mkdir -p ./solr-${ENVIRONMENT}/tnt/
mkdir -p ./solr-${ENVIRONMENT}/rhepro/
mkdir -p ./solr-${ENVIRONMENT}/weborder/

echo "Starting TnT solr export"
echo "Copying tntDelivery..."
sshpass -p "!idealist10" scp -r idealist@${SSH_HOST}:/software/idealist/portal/solr-tnt/server/solr/tntDelivery solr-${ENVIRONMENT}/tnt/tntDelivery
echo "Copying tntMasterorder..."
sshpass -p "!idealist10" scp -r idealist@$10.127.8.4:/software/idealist/portal/solr-tnt/server/solr/tntMasterorder tnt/tntMasterorder
echo "Syncing tnt data with s3 bucket..."
aws s3 sync ./solr-${ENVIRONMENT}/tnt/ $BUCKET/tnt --profile $PROFILE
echo "Completed tnt solr export."

echo "Starting RhePro solr export"
echo "Copying rpAddressBook..."
sshpass -p "!idealist10" scp -r idealist@${SSH_HOST}:/software/idealist/portal/solr-rhepro/server/solr/rpAddressBook solr-${ENVIRONMENT}/rhepro/rpAddressBook
echo "Copying rpPickupOrder..."
sshpass -p "!idealist10" scp -r idealist@${SSH_HOST}:/software/idealist/portal/solr-rhepro/server/solr/rpPickupOrder solr-${ENVIRONMENT}/rhepro/rpPickupOrder
echo "Syncing RhePro data with s3 bucket..."
aws s3 sync ./solr-${ENVIRONMENT}/rhepro/ $BUCKET/rhepro --profile $PROFILE
echo "Completed RhePro solr export."

echo "Starting Weborder solr export"
echo "Copying woAddressBook..."
sshpass -p "!idealist10" scp -r idealist@${SSH_HOST}:/software/idealist/portal/solr-weborder/server/solr/woAddressBook solr-${ENVIRONMENT}/weborder/woAddressBook
echo "Copying woShipment..."
sshpass -p "!idealist10" scp -r idealist@${SSH_HOST}:/software/idealist/portal/solr-weborder/server/solr/woShipment solr-${ENVIRONMENT}/weborder/woShipment
echo "Syncing Weborder data with s3 bucket..."
aws s3 sync ./solr-${ENVIRONMENT}/weborder/ $BUCKET/weborder --profile $PROFILE
echo "Completed Weborder solr export."

echo "Completed solr export to s3 bucket."

#! /bin/bash
BUCKET=s3://lp-test-solr-data/test
PROFILE=rfn-malz
SSH_HOST=10.220.1.14

saml2aws login --profile $PROFILE
rm -r ./solr-test/
mkdir -p ./solr-test/tnt/
mkdir -p ./solr-test/rhepro/
mkdir -p ./solr-test/weborder/

echo "Starting TnT solr export"
echo "Copying tntDelivery..."
sshpass -p "!idealist10" scp -r idealist@${SSH_HOST}:~/solr-tnt/server/solr/tntDelivery solr-test/tnt/tntDelivery
echo "Copying tntMasterorder..."
sshpass -p "!idealist10" scp -r idealist@${SSH_HOST}:~/solr-tnt/server/solr/tntMasterorder solr-test/tnt/tntMasterorder
echo "Syncing tnt data with s3 bucket..."
aws s3 sync ./solr-test/tnt/ $BUCKET/tnt --profile $PROFILE
echo "Completed tnt solr export."

echo "Starting RhePro solr export"
echo "Copying rpAddressBook..."
sshpass -p "!idealist10" scp -r idealist@${SSH_HOST}:~/solr-rhepro/server/solr/rpAddressBook solr-test/rhepro/rpAddressBook
echo "Copying rpPickupOrder..."
sshpass -p "!idealist10" scp -r idealist@${SSH_HOST}:~/solr-rhepro/server/solr/rpPickupOrder solr-test/rhepro/rpPickupOrder
echo "Syncing RhePro data with s3 bucket..."
aws s3 sync ./solr-test/rhepro/ $BUCKET/rhepro --profile $PROFILE
echo "Completed RhePro solr export."

echo "Starting Weborder solr export"
echo "Copying woAddressBook..."
sshpass -p "!idealist10" scp -r idealist@${SSH_HOST}:~/solr-weborder/server/solr/woAddressBook solr-test/weborder/woAddressBook
echo "Copying woShipment..."
sshpass -p "!idealist10" scp -r idealist@${SSH_HOST}:~/solr-weborder/server/solr/woShipment solr-test/weborder/woShipment
echo "Syncing Weborder data with s3 bucket..."
aws s3 sync ./solr-test/weborder/ $BUCKET/weborder --profile $PROFILE
echo "Completed Weborder solr export."

echo "Completed solr export to s3 bucket."

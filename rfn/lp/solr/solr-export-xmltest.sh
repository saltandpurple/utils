#! /bin/bash
BUCKET=s3://lp-test-solr-data
PROFILE=rfn-malz
SSH_HOST=10.255.182.30

saml2aws login --profile $PROFILE
rm -r ./solr-xmltest/
mkdir -p ./solr-xmltest/tnt/

echo "Starting TnT solr xmltest export"
echo "Copying tntDelivery..."
sshpass -p "!idealist10" scp -r idealist@${SSH_HOST}:~/solr-tnt-xml-test/server/solr/tntDelivery solr-xmltest/tnt/tntDelivery
echo "Copying tntMasterorder..."
sshpass -p "!idealist10" scp -r idealist@${SSH_HOST}:~/solr-tnt-xml-test/server/solr/tntMasterorder solr-xmltest/tnt/tntMasterorder
echo "Syncing tnt data with s3 bucket..."
aws s3 sync ./solr-xmltest/tnt/ $BUCKET/xmltest/tnt --profile $PROFILE
echo "Completed tnt solr xmltest export."
echo "Completed solr xmltest export to s3 bucket."

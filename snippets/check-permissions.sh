#!/bin/bash

# use all namespaces if none has been passed - otherwise use provided one
if [ $# = 0 ]; then
  namespace=-A
else
  args=("$@")
  namespace="-n ${args[0]}"
fi

# get all pods (including namespace + service account name) that haven't been assigned the default ServiceAccount
kubectl get pods $namespace -o=jsonpath={'range .items[?(@.spec.serviceAccountName!="default")]} {@.metadata.name};{@.metadata.namespace};{@.spec.serviceAccountName}{"\n"}{end}' > non-default-pods.list

# print permissions for them
echo "##################################################"
echo "Pods with non-default service accounts:"
echo "##################################################"
for Entry in $(cat non-default-pods.list)
do
  # setup constants
  RED='\033[1;31m'
  GREEN='\033[1;32m'
  YELLOW='\033[1;33m'
  NORMAL='\033[0m'
  IFS=';'
  ASTERISK='[*]'
  VALUES=( $Entry )

  # print basic properties
  echo "##############"
  echo -e "Podname:${YELLOW} ${VALUES[0]}${NORMAL}"
  echo -e "Namespace:${YELLOW} ${VALUES[1]}${NORMAL}"
  echo -e "ServiceAccount:${YELLOW} ${VALUES[2]}${NORMAL}"
  echo "Permissions:"

  # color permissions depending on their criticality
  while read -r line; do
    color=$NORMAL
    if [[ $line == *"create"*  || $line == *"use"* || $line == *"secret"* || $line == *"update"* || $line == *"patch"* || $line = *"$ASTERISK"* ]]; then
      color=$RED
    elif [[ $line == *"get"* || $line == *"list"* || $line = *"watch"* ]]; then
      color=$GREEN
    fi
    echo -e "${color}$line${NORMAL}"
  done < <(kubectl auth can-i --list --as=system:serviceaccount:"${VALUES[1]}":"${VALUES[2]}")
done

# cleanup
rm non-default-pods.list
unset IFS
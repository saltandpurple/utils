#!/usr/bin/env python3
from sys import argv
from ansible_vault import Vault
from os import remove
from shutil import move

import random
import string


def generate_ansible_password() -> str:
	vault = Vault(argv[2])
	characters = string.ascii_letters + string.digits
	password = ''.join(random.choice(characters) for i in range(32))
	print(f'Generated password: {password}')
	return password


old_file_path = argv[1]
new_file_path = argv[1] + '-new'

with open(new_file_path, 'w') as new_file:
	with open(old_file_path, 'r') as old_file:
		for line in old_file:
			new_file.write(line.replace('${REPLACE_ME}', generate_ansible_password()))

remove(old_file_path)
move(new_file_path, old_file_path)
